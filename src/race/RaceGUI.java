package race;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//
//
//import jade.core.AID;
//import java.awt.*;
//import java.awt.event.*;
import javax.swing.*;

public class RaceGUI extends JFrame {

	private Track myTrack;
	
	private JTextField lengthField, widthField, lapsField;
	
	public RaceGUI(Track myTrack){
		super(myTrack.getLocalName());
		
		this.myTrack = myTrack;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4, 2));
	
		panel.add(new JLabel("D�ugo�� toru:"));
		lengthField = new JTextField(20);
		panel.add(lengthField);
		
		panel.add(new JLabel("Szeroko�� toru:"));
		widthField = new JTextField(20);
		panel.add(widthField);
		
		panel.add(new JLabel("Ilo�� okr��e�:"));
		lapsField = new JTextField(20);
		panel.add(lapsField);
		
		getContentPane().add(panel, BorderLayout.CENTER);
		
		JButton saveButton = new JButton("Zapisz");
		saveButton.addActionListener( new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					String length = lengthField.getText().trim();
					String width = widthField.getText().trim();
					String laps = lapsField.getText().trim();
					myTrack.setSettings(length, width, laps);
					lengthField.setText("");
					widthField.setText("");
					lapsField.setText("");
				}catch(Exception ex){
					JOptionPane.showMessageDialog(RaceGUI.this,  "Niepoprawne warto�ci. "+ ex.getMessage(), "B��d", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		panel = new JPanel();
		panel.add(saveButton);
		getContentPane().add(panel, BorderLayout.SOUTH);
	}
	
	public void showGui(){
		pack();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screen.getWidth() / 2;
		int centerY = (int)screen.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}
}
