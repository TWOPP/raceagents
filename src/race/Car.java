package race;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;

public class Car extends Agent{
		private CarGUI carGui;
		private String name;
		private enum State {STOP, RUNNING, CRASH, END};
		private State activeState;
		public enum Strategy {AGRESSIVE, DEFENSIVE};
		private Strategy activeStrategy;
		//inicjalizacja
		protected void setup(){
			carGui = new CarGUI(this);
			carGui.showGui();
		}
		
		//czy�ci agenta
		protected void takeDown(){
			switch(activeState){
				case STOP:
					System.out.println("Samochod "+name+" nie wystartowa�");
					break;
				case RUNNING:
					System.out.println("Samochod "+name+" nie wystartowa�");
					break;
				case CRASH:
					System.out.println("Samochod "+name+" mia� wypadek");
					break;
				case END:
					System.out.println("Samochod "+name+" zako�czy� wy�cig");
					break;
				default:
					System.out.println("Samochod "+name+" sie popsu�");
					break;
			}
		}
		
		private class RequestPerformer extends Behaviour {

			@Override
			public void action() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public boolean done() {
				// TODO Auto-generated method stub
				return false;
			}
			
		}
		
		public void setState(State state){
			this.activeState = state;
		}
		
		
		public void setSettings(final Strategy strategy){
			addBehaviour(new OneShotBehaviour() {
				
				@Override
				public void action() {
					activeStrategy = strategy;
					name = getAgent().getLocalName();
					System.out.println("Dodano nowego agenta: "+name+". Strategia: "+activeStrategy);
					carGui.dispose();
				}
			});
		}
}
