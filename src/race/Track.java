package race;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class Track extends Agent {
	private RaceGUI raceGui;
	private int[][]track;
	private int LengthTrack, WidthTrack, Laps;
	//inicjalizacja
	@Override
	protected void setup(){
		System.out.printf("Siema %s\n", getAID().getName());
		
		raceGui = new RaceGUI(this);
		raceGui.showGui();
		
	}
		
	//czy�ci agenta
	@Override
	protected void takeDown(){
		System.out.printf("Zegnaj %s\n", getAID().getName());
	}
	
	
	private int[][] trackGenerate(){
//		int length = Integer.parseInt(args[0].toString());
//		int width = Integer.parseInt(args[1].toString());
		int length = LengthTrack;
		int width = WidthTrack;
		System.out.println("Wymiary toru: d�ugo��:"+length+" szeroko��:"+width);
		int islandLength = length - 2 * width;
        int islandWidth = (int)Math.ceil((width /2));
		int stadionWidth = 2 * width+2 + islandWidth;
        int stadionLength = (int)Math.floor((length /2));
		int[][] result = new int[stadionWidth][stadionLength];
		
        for(int i = 0; i<stadionWidth; i++){
            for (int j= 0; j < stadionLength ; j++)
            {
                if (i == 0 || i == stadionWidth - 1 || j == 0 || j == stadionLength - 1){
                	result[i][j] = '*';
                	System.out.print("*");
                }else if ((j > width && j < (stadionLength-1 - width)) && (i > width && i < (stadionWidth-1 - width))){
                    result[i][j] = '*';
                	System.out.print("*");
                }else{
                    result[i][j] = ' ';
                	System.out.print(" ");
                }
            }
        	System.out.println("");
        }
		return result;
	}
	
	public void setSettings(final String length, final String width, final String laps){
		addBehaviour(new OneShotBehaviour(){
			public void action(){
				LengthTrack = Integer.parseInt(length);
				WidthTrack = Integer.parseInt(width);
				Laps = Integer.parseInt(laps);
				trackGenerate();
				raceGui.dispose();
			}
		});
	}
	
//	private class RequestPerformer extends CyclicBehaviour {
//			
//		@Override
//		public void action() {
//			// TODO Auto-generated method stub
//			
//		}
//		
//	}
}
