package race;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import race.Car.Strategy;

public class CarGUI extends JFrame{

	private Car myCar;
	private String[] Strategies = {"Agresywna", "Defensywna"}; 
	private JTextField nameField;
	private JComboBox sList;
	
	CarGUI(Car car) {
		super(car.getLocalName());
		
		myCar = car;
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(4,4));
		
		panel.add(new JLabel("Nazwa agenta: "));
		panel.add(new JLabel(car.getLocalName()));
		
		panel.add(new JLabel("Wybierz strategie: "));
		sList = new JComboBox(Strategies);
		sList.setSelectedIndex(0);
		panel.add(sList);
		
		getContentPane().add(panel, BorderLayout.CENTER);
		
		JButton addButton = new JButton("Dodaj");
		addButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					int strategy = sList.getSelectedIndex();
					if(strategy == 0)
						myCar.setSettings(Strategy.AGRESSIVE);
					else
						myCar.setSettings(Strategy.DEFENSIVE);
				}catch(Exception ex){
					JOptionPane.showMessageDialog(CarGUI.this, "Niepoprawne warto�ci. "+ex.getMessage(), "B��d", JOptionPane.ERROR_MESSAGE);
				}
				
			}
		});
		
		panel = new JPanel();
		panel.add(addButton);
		getContentPane().add(panel, BorderLayout.SOUTH);
	}
	
	public void showGui(){
		pack();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int centerX = (int)screen.getWidth() / 2;
		int centerY = (int)screen.getHeight() / 2;
		setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
		super.setVisible(true);
	}
	
}
